defmodule ListFilterTest do
  use ExUnit.Case

  describe "call/1" do
    test "returns the number of odd numbers" do
      list = ["1", "3", "6", "43", "banana", "6", "abc"]

      result = ListFilter.call(list)

      expected_result = 3

      assert expected_result == result
    end
  end
end
