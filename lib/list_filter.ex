defmodule ListFilter do
  @moduledoc """
  Documentation for `ListFilter`.
  """

  def call(list) do
    Enum.count(odd_numbers(numbers(list)))
  end

  defp odd_numbers(list) do
    Enum.filter(list, fn num -> rem(num, 2) != 0 end)
  end

  defp numbers(list) do
    Enum.flat_map(list, fn string ->
      case Integer.parse(string) do
        {num, _rest} -> [num]
        :error -> []
      end
    end)
  end
end
